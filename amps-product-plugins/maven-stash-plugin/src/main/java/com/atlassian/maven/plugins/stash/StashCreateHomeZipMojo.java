package com.atlassian.maven.plugins.stash;

import com.atlassian.maven.plugins.amps.CreateHomeZipMojo;

import org.apache.maven.plugins.annotations.Mojo;

/**
 * @since 3.10
 */
@Mojo(name = "create-home-zip")
public class StashCreateHomeZipMojo extends CreateHomeZipMojo
{
}
